using System.Threading.Tasks;
using InternalApi.Models.Supplier;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InternalApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SupplierController : Controller
    {
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create([FromBody] CreateSupplierConfiguration configuration)
        {
            return Ok();
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Suppliers()
        {
            return Ok();
        }

        [HttpGet]
        [Route("{supplier}")]
        public async Task<IActionResult> Supplier(string supplier)
        {
            return Ok();
        }
    }
}