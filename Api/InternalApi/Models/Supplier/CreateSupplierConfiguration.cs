using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Foundation.ModelGeneration;

namespace InternalApi.Models.Supplier
{
    public class CreateSupplierConfiguration
    {
        [Required]
        public string SupplierName { get; set; }

        [Required, MinLength(1)]
        public List<ClassGenerationModel> Classes { get; set; }
    }

    public class ClassGenerationModel
    {
        public string TypeName { get; set; }

        public List<PropertyGenerationModel> Properties { get; set; }
    }

    public class PropertyGenerationModel
    {
        public string Name { get; set; }

        public string Type { get; set; }
    }
}