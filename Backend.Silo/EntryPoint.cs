﻿using System;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Foundation.DependencyInjection;
using Foundation.Mediator;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Middleware.Grains.Grains;
using Middleware.Interfaces.GrainInterfaces;
using Orleans;
using Orleans.Hosting;

namespace Backend.Silo
{
    public class EntryPoint
    {
        public static Task Main(string[] args)
        {
            Console.Title = nameof(EntryPoint);

            return new HostBuilder()
                .UseServiceProviderFactory(context => new AutofacServiceProviderFactory(container =>
                {
                    container.RegisterModule<RegistoryAdapator<MediatorRegistory>>();
                }))
                .UseOrleans(builder =>
                {
                    builder.UseLocalhostClustering()
                        .UseDashboard(a => a.Port = 8080)
                        .ConfigureApplicationParts(manager =>
                        {
                            manager.AddApplicationPart(typeof(IntegrationGrain).Assembly).WithReferences();
                        });
                    builder.AddStartupTask(async (provider, token) =>
                    {
                        var factory = provider.GetService<IGrainFactory>();
                        var integration = factory.GetGrain<IIntegrationGrain>("0");
                        await integration.Book();
                    });
                })
                .ConfigureLogging(builder => builder.AddConsole())
                .RunConsoleAsync();
        }
    }
}