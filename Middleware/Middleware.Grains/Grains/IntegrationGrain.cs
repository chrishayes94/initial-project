using System;
using System.Threading.Tasks;
using Foundation.Mediator;
using Middleware.Interfaces.GrainInterfaces;
using Orleans;

namespace Middleware.Grains.Grains
{
    public class IntegrationGrain : Grain, IIntegrationGrain
    {
        private readonly IMediator _mediator;

        public IntegrationGrain(IMediator mediator)
        {
            _mediator = mediator;
        }

        public Task Book()
        {
            throw new NotImplementedException();
        }

        public Task Label()
        {
            return Task.CompletedTask;
        }

        public Task Cancel()
        {
            return Task.CompletedTask;
        }

        public Task<Func<Task>> Other(string actionName)
        {
            return Task.FromResult<Func<Task>>(() => Task.CompletedTask);
        }
    }
}