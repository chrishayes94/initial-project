using System;
using System.Threading.Tasks;
using Orleans;

namespace Middleware.Interfaces.GrainInterfaces
{
    public interface IIntegrationGrain : IGrainWithStringKey
    {
        Task Book();

        Task Label();

        Task Cancel();

        Task<Func<Task>> Other(string actionName);
    }
}