using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using Foundation.Core.Storage;
using Orleans;

namespace Foundation.Core.ApiMembership
{
    [Serializable]
    [DebuggerDisplay("ApiAddress {ToString()}")]
    public class ApiAddress : IEquatable<ApiAddress>, IComparable<ApiAddress>, IComparable
    {
        internal static readonly int SizeBytes = 24; // 16 for address, 4 for the port, 4 for the generation

        public static ApiAddress Zero { get; private set; }

        private const int InternCacheInitialSize = InternalConstants.SizeMedium;
        private static readonly TimeSpan InternCacheCleanupInterval = TimeSpan.Zero;
        
        private int _hashCode = 0;
        private bool _hashCodeSet = false;

        [NonSerialized] private List<uint> _uniformHashCache;

        public IPEndPoint EndPoint { get; private set; }
        public int Generation { get; private set; }

        private const char Seperator = '@';

        private static readonly DateTime Epoch = new DateTime(2010, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private static readonly Interner<ApiAddress, ApiAddress> ApiAddressInterningCache;

        static ApiAddress()
        {
            ApiAddressInterningCache =
                new Interner<ApiAddress, ApiAddress>(InternCacheInitialSize, InternCacheCleanupInterval);
            var sa = new ApiAddress(new IPEndPoint(0, 0), 0);
            Zero = ApiAddressInterningCache.Intern(sa, sa);
        }

        public static ApiAddress New(IPEndPoint ep, int gen)
        {
            var sa = new ApiAddress(ep, gen);
            return ApiAddressInterningCache.Intern(sa, sa);
        }

        public static int AllocateNewGeneration()
        {
            var elapsed = (DateTime.UtcNow.Ticks - Epoch.Ticks) / TimeSpan.TicksPerSecond;
            return unchecked((int) elapsed);
        }

        public static ApiAddress FromParsableString(string addr)
        {
            var atSign = addr.LastIndexOf(Seperator);
            if (atSign < 0)
                throw new FormatException("Invalid string ApiAddress: " + addr);

            var epString = addr.Substring(0, atSign);
            var genString = addr.Substring(atSign + 1);

            var lastColon = epString.LastIndexOf(':');
            if (lastColon < 0) throw new FormatException("Invalid string ApiAddress: " + addr);

            var hostString = epString.Substring(0, lastColon);
            var portString = epString.Substring(lastColon + 1);
            var host = IPAddress.Parse(hostString);
            var port = int.Parse(portString);
            return New(new IPEndPoint(host, port), int.Parse(genString));
        }

        private static int CalculateIdHash(string text)
        {
            var sha = SHA256.Create();
            var hash = 0;
            try
            {
                var data = Encoding.Unicode.GetBytes(text);
                var result = sha.ComputeHash(data);
                hash = result.Select((t, i) => (t << 24) | (result[i + 1] << 16) | (result[i + 2] << 8) | result[i + 3])
                    .Aggregate(hash, (current, tmp) => current ^ tmp);
            }
            finally
            {
                sha.Dispose();
            }

            return hash;
        }
        
        public ApiAddress(IPEndPoint endPoint, int gen)
        {
            if (endPoint.Address.IsIPv4MappedToIPv6)
                endPoint = new IPEndPoint(endPoint.Address.MapToIPv4(), endPoint.Port);

            EndPoint = endPoint;
            Generation = gen;
        }

        public bool IsClient => Generation < 0;

        public bool Equals(ApiAddress other)
        {
            return other != null && EndPoint.Address.Equals(other.EndPoint.Address) && EndPoint.Port == other.EndPoint.Port &&
                   Generation == other.Generation;
        }

        public int CompareTo(ApiAddress other)
        {
            if (other == null) return 1;

            var comp = Generation.CompareTo(other.Generation);
            if (comp != 0) return comp;

            comp = EndPoint.Port.CompareTo(other.EndPoint.Port);
            if (comp != 0) return comp;

            return CompareIpAddresses(EndPoint.Address, other.EndPoint.Address);
        }

        private static int CompareIpAddresses(IPAddress @this, IPAddress other)
        {
            var returnVal = 0;
            if (@this.AddressFamily == other.AddressFamily)
            {
                var b1 = @this.GetAddressBytes();
                var b2 = other.GetAddressBytes();

                for (var i = 0; i < b1.Length; i++)
                {
                    if (b1[i] < b2[i])
                    {
                        returnVal = -1;
                        break;
                    }

                    if (b1[i] <= b2[i]) continue;

                    returnVal = 1;
                    break;
                }
            }
            else returnVal = @this.AddressFamily.CompareTo(other.AddressFamily);
            
            return returnVal;
        }

        public int CompareTo(object obj) => CompareTo((ApiAddress)obj);

        public override int GetHashCode() => EndPoint.GetHashCode() ^ Generation.GetHashCode();

        public int GetConsistentHashCode()
        {
            if (_hashCodeSet) return _hashCode;

            var apiAddressInfoHash = EndPoint + Generation.ToString(CultureInfo.InvariantCulture);
            _hashCode = CalculateIdHash(apiAddressInfoHash);
            _hashCodeSet = true;
            return _hashCode;
        }

        public List<uint> GetUniformHashCodes(int numHashes)
        {
            if (_uniformHashCache != null) return _uniformHashCache;

            _uniformHashCache = GetUniformHashCodesImpl(numHashes);
            return _uniformHashCache;
        }

        private List<uint> GetUniformHashCodesImpl(int numHashes)
        {
            var hashes = new List<uint>();
            var bytes = new byte[16 + sizeof(int) + sizeof(int) + sizeof(int)];
            var tmpInt = new int[1];

            for (var i = 0; i < bytes.Length; i++)
                bytes[i] = 9;

            if (EndPoint.AddressFamily == AddressFamily.InterNetwork)
            {
                for (var i = 0; i < 12; i++)
                    bytes[i] = 0;
                Buffer.BlockCopy(EndPoint.Address.GetAddressBytes(), 0, bytes, 12, 4);
            }
            else
                Buffer.BlockCopy(EndPoint.Address.GetAddressBytes(), 0, bytes, 0, 16);

            var offset = 16;
            
            // Port
            tmpInt[0] = EndPoint.Port;
            Buffer.BlockCopy(tmpInt, 0, bytes, offset, sizeof(int));
            offset += sizeof(int);
            
            // Generation
            tmpInt[0] = Generation;
            Buffer.BlockCopy(tmpInt, 0, bytes, offset, sizeof(int));
            offset += sizeof(int);

            for (var extraBit = 0; extraBit < numHashes; extraBit++)
            {
                tmpInt[0] = extraBit;
                Buffer.BlockCopy(tmpInt, 0, bytes, offset, sizeof(int));
                hashes.Add(JenkinsHash.ComputeHash(bytes));
            }

            return hashes;
        }
        
        internal bool Matches(ApiAddress other)
        {
            return other != null && EndPoint.Address.Equals(other.EndPoint.Address) &&
                   (EndPoint.Port == other.EndPoint.Port) &&
                   ((Generation == other.Generation) || (Generation == 0) || other.Generation == 0);
        }
        
        public string ToParsableString() => $"{EndPoint.Address}:{EndPoint.Port}{Seperator}{Generation}";

        public string ToLongString() => ToString();

        public string ToStringWithHashCode() => $"{ToString()}/x{GetConsistentHashCode(),8:X8}";

        public override string ToString() => $"{(IsClient ? "C" : "S")}{EndPoint}:{Generation}";
    }
}