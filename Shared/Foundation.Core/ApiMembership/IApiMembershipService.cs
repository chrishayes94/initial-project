using System.Threading.Tasks;
using Orleans;

namespace Foundation.Core.ApiMembership
{
    public interface IApiMembershipService : ISystemTarget
    {
        Task ApiStatusChangeNotification(ApiAddress updateApi, ApiStatus status);
        
    }
}