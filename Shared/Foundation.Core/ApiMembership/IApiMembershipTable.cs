using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Orleans;
using Orleans.Concurrency;

namespace Foundation.Core.ApiMembership
{
    public interface IApiMembershipTable
    {
        Task InitializeApiTable();

        Task DeleteApiTableEntries(string clusterId);

        Task CleanupDefunctApiIntegrations(DateTimeOffset beforeDate);

        Task<ApiMembershipTableData> ReadRow(ApiAddress key);

        Task<ApiMembershipTableData> ReadAll();

        Task<bool> InsertRow(ApiMembershipEntry entry, TableVersion tableVersion);

        Task<bool> UpdateRow(ApiMembershipEntry entry, string etag, TableVersion version);
    }

    [Unordered]
    public interface IApiMembershipSystemTarget : IApiMembershipTable, ISystemTarget
    {
    }

    [Serializable]
    [Immutable]
    public struct TableVersion
    {
        public int Version { get; private set; }
        
        public string VersionETag { get; private set; }

        public TableVersion(int version, string versionETag)
        {
            Version = version;
            VersionETag = versionETag;
        }

        public TableVersion Next() => new TableVersion(Version + 1, VersionETag);

        public override string ToString() => $"<{Version}, {VersionETag}>";
    }
    
    [Serializable]
    public class ApiMembershipTableData
    {
        public IReadOnlyList<Tuple<ApiMembershipEntry, string>> Members { get; private set; }

        public TableVersion Version { get; private set; }

        public ApiMembershipTableData(List<Tuple<ApiMembershipEntry, string>> list, TableVersion version)
        {
            list.Sort((x, y) =>
            {
                if (x.Item1.Status == ApiStatus.Dead) return 1;
                if (y.Item1.Status == ApiStatus.Dead) return -1;
                return string.Compare(x.Item1.ApiName, y.Item1.ApiName, StringComparison.Ordinal);
            });
            Members = list.AsReadOnly();
            Version = version;
        }

        public ApiMembershipTableData(Tuple<ApiMembershipEntry, string> tuple, TableVersion version)
        {
            Members = new List<Tuple<ApiMembershipEntry, string>> {tuple}.AsReadOnly();
            Version = version;
        }

        public ApiMembershipTableData(TableVersion version)
        {
            Members = new List<Tuple<ApiMembershipEntry, string>>();
            Version = version;
        }

        public Tuple<ApiMembershipEntry, string> Get(ApiAddress address) =>
            Members.First(tuple => tuple.Item1.ApiAddress.Equals(address));

        public bool Contains(ApiAddress address) => Members.Any(tuple => tuple.Item1.ApiAddress.Equals(address));

        public ApiMembershipTableData WithoutDuplicateDeads()
        {
            var dead = new Dictionary<IPEndPoint, Tuple<ApiMembershipEntry, string>>();

            foreach (var next in Members.Where(e => e.Item1.Status == ApiStatus.Dead))
            {
                var ipEndpoint = next.Item1.ApiAddress.EndPoint;
                if (!dead.TryGetValue(ipEndpoint, out var prev))
                    dead[ipEndpoint] = next;
                else
                {
                    if (next.Item1.ApiAddress.Generation.CompareTo(prev.Item1.ApiAddress.Generation) > 0)
                        dead[ipEndpoint] = next;
                }
            }

            var all = dead.Values.ToList();
            all.AddRange(Members.Where(i => i.Item1.Status != ApiStatus.Dead));
            return new ApiMembershipTableData(all, Version);
        }

        internal Dictionary<ApiAddress, ApiStatus> getApiStatuses(Func<ApiStatus, bool> filter)
        {
            var result = new Dictionary<ApiAddress, ApiStatus>();
            foreach (var member in Members)
            {
                var entry = member.Item1;
                if (filter(entry.Status)) result[entry.ApiAddress] = entry.Status;
            }

            return result;
        }
        
        public override string ToString()
        {
            var created = Members.Count(e => e.Item1.Status == ApiStatus.Created);
            var active = Members.Count(e => e.Item1.Status == ApiStatus.Active);
            var shuttingDown = Members.Count(e => e.Item1.Status == ApiStatus.ShuttingDown);
            var stopping = Members.Count(e => e.Item1.Status == ApiStatus.Stopping);
            var dead = Members.Count(e => e.Item1.Status == ApiStatus.Dead);
            var updating = Members.Count(e => e.Item1.Status == ApiStatus.Updating);

            var otherCounts =
                $"{(created > 0 ? ", " + created + " are Created" : "")}{(updating > 0 ? ", " + updating + " are Updating" : "")}{(shuttingDown > 0 ? ", " + shuttingDown + " are ShuttingDown" : "")}{(stopping > 0 ? ", " + stopping + " are Stopping" : "")}";

            return
                $"{Members.Count} Apis, {active} are Active, {dead} are Dead{otherCounts}, {updating} are Updating, Version={Version}. All Apis: {Members.SelectMany(a => a.Item1.ToFullString())}";
        }
    }

    [Serializable]
    public class ApiMembershipEntry
    {
        public ApiAddress ApiAddress { get; set; }

        public ApiStatus Status { get; set; }

        public int ProxyPort { get; set; }

        public string HostName { get; set; }

        public string ApiName { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime AmAliveTime { get; set; }

        internal ApiMembershipEntry Copy()
        {
            return new ApiMembershipEntry
            {
                Status = Status,
                ApiAddress = ApiAddress,
                ApiName = ApiName,
                HostName = HostName,
                ProxyPort = ProxyPort,
                StartTime = StartTime,
                AmAliveTime = AmAliveTime
            };
        }

        internal ApiMembershipEntry WithStatus(ApiStatus status)
        {
            var updated = Copy();
            updated.Status = status;
            return updated;
        }

        public override string ToString() =>
            $"ApiAddress={ApiAddress.ToLongString()} ApiName={ApiName} Status={Status}";

        public string ToFullString(bool full = false)
        {
            if (!full) return ToString();
            return
                $"[ApiAddress={ApiAddress} ApiName={ApiName} Status={Status} ProxyPort={ProxyPort} StartTime={StartTime} AmAliveTime={AmAliveTime}]";
        }
    }
}