namespace Foundation.Core
{
    public enum ApiStatus
    {
        None = 0,
        
        Created = 1,
        
        Active = 2,
        
        ShuttingDown = 3,
        
        Stopping = 4,
        
        Dead = 5,
        
        Updating = 6
    }

    public static class ApiStatusExtensions
    {
        public static bool IsTerminating(this ApiStatus status) =>
            status == ApiStatus.ShuttingDown || status == ApiStatus.Stopping || status == ApiStatus.Dead;

        public static bool IsUnavailable(this ApiStatus status) =>
            status == ApiStatus.None || status == ApiStatus.Updating;
    }
}