using System;
using System.Collections.Immutable;
using Foundation.Core.ApiMembership;
using Orleans.Runtime;

namespace Foundation.Core.Runtime
{
    internal sealed class ApiMembershipTableSnapshot
    {
        public ApiMembershipVersion Version { get; }
        public ImmutableDictionary<ApiAddress, ApiMembershipEntry> Entries { get; }

        public ApiMembershipTableSnapshot(ApiMembershipVersion version,
            ImmutableDictionary<ApiAddress, ApiMembershipEntry> entries)
        {
            Version = version;
            Entries = entries;
        }

        public static ApiMembershipTableSnapshot Create(ApiMembershipTableData table)
        {
            if (table is null) throw new ArgumentNullException(nameof(table));

            var entries = ImmutableDictionary.CreateBuilder<ApiAddress, ApiMembershipEntry>();
            if (!(table.Members is null))
            {
                foreach (var item in table.Members)
                {
                    var entry = item.Item1;
                    entries.Add(entry.ApiAddress, entry);
                }
            }

            var version = table.Version.Version == 0 && table.Version.VersionETag == "0"
                ? ApiMembershipVersion.MinValue
                : new ApiMembershipVersion(table.Version.Version);
            return new ApiMembershipTableSnapshot(version, entries.ToImmutable());
        }
        
        
    }
}