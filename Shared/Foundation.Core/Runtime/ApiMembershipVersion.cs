using System;
using Foundation.Core.ApiMembership;

namespace Foundation.Core.Runtime
{
    [Serializable]
    public class ApiMembershipVersion : IComparable<ApiMembershipVersion>, IEquatable<ApiMembershipVersion>
    {
        public static ApiMembershipVersion MinValue => new ApiMembershipVersion(long.MinValue);

        public static bool operator ==(ApiMembershipVersion left, ApiMembershipVersion right) =>
            left.Value == right.Value;

        public static bool operator !=(ApiMembershipVersion left, ApiMembershipVersion right) =>
            left.Value != right.Value;

        public static bool operator >=(ApiMembershipVersion left, ApiMembershipVersion right) =>
            left.Value >= right.Value;

        public static bool operator <=(ApiMembershipVersion left, ApiMembershipVersion right) =>
            left.Value <= right.Value;

        public static bool operator <(ApiMembershipVersion left, ApiMembershipVersion right) =>
            left.Value < right.Value;

        public static bool operator >(ApiMembershipVersion left, ApiMembershipVersion right) =>
            left.Value > right.Value;  
        
        public ApiMembershipVersion(long version)
        {
            Value = version;
        }

        public long Value { get; private set; }

        public int CompareTo(ApiMembershipVersion other) => Value.CompareTo(other.Value);

        public bool Equals(ApiMembershipVersion other) => Value == other.Value;

        public override bool Equals(object obj) => obj is ApiMembershipVersion other && Equals(other);

        public override int GetHashCode() => Value.GetHashCode();

        public override string ToString() => Value.ToString();
    }
}