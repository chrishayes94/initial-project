using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;

namespace Foundation.Core.Storage
{
    internal static class InternalConstants
    {
        public const int SizeSmall = 67;
        public const int SizeMedium = 1117;
        public const int SizeLarge = 143357;
        public const int SizeXLarge = 2293757;

        public static readonly TimeSpan DefaultCacheCleanupFreq = TimeSpan.FromMinutes(10);
    }
    
    internal class Interner<TK, T> : IDisposable where T : class
    {
        private readonly TimeSpan _cacheCleanupInterval;
        private readonly Timer _cacheCleanupTimer;

        [NonSerialized] private readonly ConcurrentDictionary<TK, WeakReference<T>> _internalCache;

        public Interner() : this(InternalConstants.SizeSmall)
        {
        }

        public Interner(int initialSize) : this(initialSize, Timeout.InfiniteTimeSpan)
        {
        }

        public Interner(int initalSize, TimeSpan cleanupFreq)
        {
            if (initalSize <= 0) initalSize = InternalConstants.SizeMedium;
            var concurrencyLevel = Environment.ProcessorCount * 4;

            _internalCache = new ConcurrentDictionary<TK, WeakReference<T>>(concurrencyLevel, initalSize);

            _cacheCleanupInterval = (cleanupFreq <= TimeSpan.Zero) ? Timeout.InfiniteTimeSpan : cleanupFreq;

            if (Timeout.InfiniteTimeSpan != _cacheCleanupInterval)
                _cacheCleanupTimer = new Timer(InternCacheCleanupTimerCallback, null, _cacheCleanupInterval,
                    _cacheCleanupInterval);
        }

        public T FindOrCreate(TK key, Func<TK, T> creatorFunc)
        {
            T result;

            _internalCache.TryGetValue(key, out var cacheEntry);

            if (cacheEntry == null)
            {
                result = creatorFunc(key);
                cacheEntry = new WeakReference<T>(result);
                _internalCache[key] = cacheEntry;
                return result;
            }

            cacheEntry.TryGetTarget(out result);
            if (result == null)
            {
                result = creatorFunc(key);
                cacheEntry.SetTarget(result);
                _internalCache[key] = cacheEntry;
            }

            return result;
        }

        public bool TryFind(TK key, out T obj)
        {
            obj = null;
            return _internalCache.TryGetValue(key, out var cacheEntry) && cacheEntry != null &&
                   cacheEntry.TryGetTarget(out obj);
        }

        public T Intern(TK key, T obj)
        {
            T result;

            _internalCache.TryGetValue(key, out var cacheEntry);

            if (cacheEntry == null)
            {
                result = obj;
                cacheEntry = new WeakReference<T>(result);
                _internalCache[key] = cacheEntry;
                return result;
            }

            cacheEntry.TryGetTarget(out result);
            if (result == null)
            {
                result = obj;
                cacheEntry.SetTarget(result);
                _internalCache[key] = cacheEntry;
            }

            return result;
        }

        public void StopAndClear()
        {
            _internalCache.Clear();
            _cacheCleanupTimer?.Dispose();
        }

        public List<T> Values()
        {
            var values = new List<T>();
            foreach (var e in _internalCache)
            {
                if (e.Value != null && e.Value.TryGetTarget(out var value))
                    values.Add(value);
            }

            return values;
        }

        private void InternCacheCleanupTimerCallback(object state)
        {
            foreach (var e in _internalCache)
            {
                if (e.Value == null || e.Value.TryGetTarget(out var ignored) == false)
                    _internalCache.TryRemove(e.Key, out _);
            }
        }

        public void Dispose()
        {
            _cacheCleanupTimer?.Dispose();
        }
    }
}