using Autofac;

namespace Foundation.DependencyInjection
{
    public class AutofacContainerProvider : IContainerProvider
    {
        public AutofacContainerProvider()
        {
            Container = new ContainerBuilder();
            Container.RegisterModule<AutofacModule>();
        }

        public ContainerBuilder Container { get; }

        public IRegistoryBuilder Builder() => new AutofacRegistory(Container);

        public IContainerScope Build() => new AutofacScope(Container.Build());
    }
}