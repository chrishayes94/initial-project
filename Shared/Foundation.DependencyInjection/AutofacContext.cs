using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Autofac;

namespace Foundation.DependencyInjection
{
    public class AutofacContext : IRegistoryContext
    {
        private readonly IComponentContext _context;

        public AutofacContext(IComponentContext context)
        {
            _context = context;
        }

        public T Resolve<T>() where T : class => _context.Resolve<T>();

        public T ResolveNamed<T>(string name) where T : class => _context.ResolveNamed<T>(name);
    }
}