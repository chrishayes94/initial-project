using Autofac;

namespace Foundation.DependencyInjection
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AutofacScope>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}