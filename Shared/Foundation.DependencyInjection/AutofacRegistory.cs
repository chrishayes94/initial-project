using System;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;

namespace Foundation.DependencyInjection
{
    public class AutofacRegistory : IRegistoryBuilder
    {
        private readonly ContainerBuilder _builder;

        public AutofacRegistory(ContainerBuilder builder)
        {
            _builder = builder;
        }

        public IRegistoryBuilder IncludeModule<T>() where T : IRegistory, new()
        {
            _builder.RegisterModule<RegistoryAdapator<T>>();
            return this;
        }

        public IRegistoryBuilder Scan(Assembly assembly, Action<IRegistoryScanner> config)
        {
            var expression = _builder.RegisterAssemblyTypes(assembly);
            var scanner = new AutofacRegistoryScanner(this, expression);
            config(scanner);
            return this;
        }

        public IRegistoryBuilder Scan(Assembly[] assemblies, Action<IRegistoryScanner> config)
        {
            foreach (var ass in assemblies)
                Scan(ass, config);
            return this;
        }

        public IRegistoryBuilder Transient(Type serviceType, Func<IRegistoryContext, object> activator)
        {
            _builder.Register(ctx => activator(new AutofacContext(ctx)))
                .As(serviceType)
                .InstancePerLifetimeScope();
            return this;
        }

        public IRegistoryBuilder Singleton<T>()
        {
            _builder.RegisterType<T>()
                .AsSelf()
                .SingleInstance();
            return this;
        }

        public IRegistoryBuilder Singleton<T>(Func<IRegistoryContext, T> p)
        {
            _builder.Register(ctx => p(new AutofacContext(ctx)))
                .As<T>()
                .SingleInstance();
            return this;
        }

        public IRegistoryBuilder SingletonIfNone<T>(Func<IRegistoryContext, T> p)
        { 
            _builder.Register(ctx => p(new AutofacContext(ctx)))
                .As<T>()
                .PreserveExistingDefaults()
                .SingleInstance();
            return this;
        }

        public IRegistoryBuilder Singleton<TImplementation, TInterface>() where TImplementation : TInterface
        {
            _builder.RegisterType<TImplementation>()
                .As<TInterface>()
                .SingleInstance();
            return this;
        }

        public IRegistoryBuilder SingletonIfNone<TImplementation, TInterface>(
            Func<IRegistoryContext, Task<TImplementation>> p) where TImplementation : TInterface
        {
            _builder.Register(ctx => p(new AutofacContext(ctx)).ConfigureAwait(false).GetAwaiter().GetResult())
                .As<TInterface>()
                .PreserveExistingDefaults()
                .SingleInstance();
            return this;
        }

        public IRegistoryBuilder Transient(Type concreteType, Type serviceType)
        {
            _builder.RegisterType(concreteType)
                .As(serviceType)
                .InstancePerLifetimeScope();
            return this;
        }

        public IRegistoryBuilder Transient<T>()
        {
            _builder.RegisterType<T>().InstancePerLifetimeScope().AsSelf();
            return this;
        }

        public IRegistoryBuilder Transient<T>(Func<IRegistoryContext, T> p)
        {
            _builder.Register(ctx => p(new AutofacContext(ctx)))
                .As<T>()
                .InstancePerLifetimeScope();
            return this;
        }

        public IRegistoryBuilder TransientGeneric(Type source, Type target)
        {
            _builder.RegisterGeneric(source)
                .As(target)
                .InstancePerLifetimeScope();
            return this;
        }

        public IRegistoryBuilder Transient<TImplementation, TInterface>() where TImplementation : TInterface
        {
            _builder.RegisterType<TImplementation>().InstancePerLifetimeScope().As<TInterface>();
            return this;
        }

        public IRegistoryBuilder Transient<TImplementation, TInterface>(Func<IRegistoryContext, TInterface> action) where TImplementation : TInterface
        {
            _builder.Register(ctx => action(new AutofacContext(ctx)))
                .As<TInterface>()
                .InstancePerLifetimeScope();
            return this;
        }

        public IRegistoryBuilder Transient<TImplementation, TInterface>(Func<IRegistoryContext, Task<TInterface>> action) where TImplementation : TInterface
        {
            _builder.Register(ctx => action(new AutofacContext(ctx)).ConfigureAwait(false).GetAwaiter().GetResult())
                .As<TInterface>()
                .InstancePerLifetimeScope();
            return this;
        }
    }
}