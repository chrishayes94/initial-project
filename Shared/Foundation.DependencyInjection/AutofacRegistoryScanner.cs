using System;
using Autofac;
using Autofac.Builder;
using Autofac.Features.Scanning;

namespace Foundation.DependencyInjection
{
    public class AutofacRegistoryScanner : IRegistoryScanner
    {
        private AutofacRegistory autofacRegistory;
        private IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> expression;

        public AutofacRegistoryScanner(AutofacRegistory autofacRegistory,
            IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> expression)
        {
            this.autofacRegistory = autofacRegistory;
            this.expression = expression;
        }

        public void AssignableAsSelf<T>() => expression.AssignableTo<T>().AsSelf();

        public void ByNameAsSelf(string partOfName) =>
            expression.Where(el => el.FullName.Contains(partOfName))
                .AsSelf();

        public IRegistoryScanner Except<T>()
        {
            return new AutofacRegistoryScanner(autofacRegistory, expression.Except<T>());
        }

        public IRegistoryScanner Ignore(string typeOrName)
        {
            return new AutofacRegistoryScanner(autofacRegistory,
                expression.Where(t => !t.Name.ToLower().Contains(typeOrName.ToLower())));
        }

        public void ClosedTypeAsSelf(Type type) =>
            expression.AsClosedTypesOf(type)
                .AsSelf();

        public void ImplementedInterfaces() =>
            expression.AsImplementedInterfaces();
    }
}