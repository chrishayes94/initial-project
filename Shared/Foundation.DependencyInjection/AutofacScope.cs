using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Autofac.Core;

namespace Foundation.DependencyInjection
{
    public static class AutofacExt
    {
        public static T[] ResolveMany<T>(this IComponentContext self)
        {
            return self.Resolve<IEnumerable<T>>().ToArray();
        }

        public static object[] ResolveMany(this IComponentContext self, Type type)
        {
            var enumerableOfType = typeof(IEnumerable<>).MakeGenericType(type);
            return (object[])self.ResolveService(new TypedService(enumerableOfType));
        }
    }


    public class AutofacScope : IContainerScope
    {
        private ILifetimeScope _scope;

        public AutofacScope(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public ILifetimeScope Scope => _scope;

        public object Resolve(Type handlerType)
        {
            if (_scope.IsRegistered(handlerType))
                return _scope.Resolve(handlerType);

            return null;
        }

        public IEnumerable<T> ResolveMany<T>()
        {
            return _scope.ResolveMany<T>();
        }

        public T Resolve<T>() where T : class
        {
            if (_scope.IsRegistered<T>())
                return _scope.Resolve<T>();

            return null;
        }

        public IContainerScope ChildScope()
        {
            var child = _scope.BeginLifetimeScope();
            return new AutofacScope(child);
        }
    }
}