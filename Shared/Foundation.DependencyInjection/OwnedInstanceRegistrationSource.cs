using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Builder;
using Autofac.Core;
using Autofac.Features.OwnedInstances;

namespace Foundation.DependencyInjection
{
    public static class TypeExtentions
    {
        public static bool IsGenericTypeDefinedBy(this Type @this, Type openGeneric)
        {
            if (@this == null) throw new ArgumentNullException(nameof(@this));
            if (openGeneric == null) throw new ArgumentNullException(nameof(openGeneric));

            return !@this.GetTypeInfo().ContainsGenericParameters && @this.GetTypeInfo().IsGenericType && @this.GetGenericTypeDefinition() == openGeneric;
        }
    }
    
    public class OwnedInstanceRegistrationSource : IRegistrationSource
    {
        /// <summary>
        /// Retrieve registrations for an unregistered service, to be used
        /// by the container.
        /// </summary>
        /// <param name="service">The service that was requested.</param>
        /// <param name="registrationAccessor">A function that will return existing registrations for a service.</param>
        /// <returns>Registrations providing the service.</returns>
        public IEnumerable<IComponentRegistration> RegistrationsFor(Service service,
            Func<Service, IEnumerable<IComponentRegistration>> registrationAccessor)
        {
            if (service == null) throw new ArgumentNullException("service");
            if (registrationAccessor == null) throw new ArgumentNullException("registrationAccessor");

            var ts = service as IServiceWithType;
            if (ts == null || !ts.ServiceType.IsGenericTypeDefinedBy(typeof(Owned<>)))
                return Enumerable.Empty<IComponentRegistration>();

            var ownedInstanceType = ts.ServiceType.GetTypeInfo().GenericTypeArguments.First();
            var ownedInstanceService = ts.ChangeType(ownedInstanceType);

            return registrationAccessor(ownedInstanceService)
                .Select(r =>
                {
                    var rb = RegistrationBuilder.ForDelegate(ts.ServiceType, (c, p) =>
                        {
                            var lifetime = c.Resolve<ILifetimeScope>().BeginLifetimeScope(ownedInstanceService);
                            try
                            {
                                var value = lifetime.ResolveComponent(new ResolveRequest(service, r, p));
                                return Activator.CreateInstance(ts.ServiceType, new[] {value, lifetime});
                            }
                            catch
                            {
                                lifetime.Dispose();
                                throw;
                            }
                        })
                        .ExternallyOwned()
                        .As(service)
                        .Targeting(r, IsAdapterForIndividualComponents);

                    return rb.CreateRegistration();
                });
        }

        public bool IsAdapterForIndividualComponents => true;

        public override string ToString()
        {
            return "System.Owned<> Support";
        }
    }
}