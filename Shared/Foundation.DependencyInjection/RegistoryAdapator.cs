using Autofac;

namespace Foundation.DependencyInjection
{
    public class RegistoryAdapator<T> : Module where T : IRegistory, new()
    {
        protected override void Load(ContainerBuilder builder)
        {
            var registory = new T();
            registory.Load(new AutofacRegistory(builder));
        }
    }
}