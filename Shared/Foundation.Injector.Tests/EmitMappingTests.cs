using System.Collections.Generic;
using Foundation.ModelGeneration;
using Xunit;

namespace Foundation.Injector.Tests
{
    public class EmitMappingTests
    {
        [Fact]
        public void WhenUsingEmitTypes_ShouldMapUsingIMapper()
        {
            // Arrange
            var piece1Type = CustomTypeBuilder.CreateDynamicType("WhenUsingEmitTypes_ShouldMapUsingIMapper", Piece1);
            var piece2Type = CustomTypeBuilder.CreateDynamicType("WhenUsingEmitTypes_ShouldMapUsingIMapper", Piece2);

            var piece1 = piece1Type.CreateObject()
                .SetValue("Length", 1.25M);
            var piece2 = piece2Type.CreateObject();
            
            // Act
            Mapper.Instance.AddMap((source, target) => source.SetValue("Length", target.GetValue("Length")), piece1Type,
                piece2Type);
            
            // Assert
            var result = piece2.InjectFrom(piece1);
            Assert.Equal(piece1.GetValue("Length"), result.GetValue("Length"));
;        }

        [Fact]
        public void WhenNoMapping_ShouldTryToMapWithSameNames()
        {
            // Arrange
            var piece1Type = CustomTypeBuilder.CreateDynamicType("WhenNoMapping_ShouldTryToMapWithSameNames", Piece1);
            var piece2Type = CustomTypeBuilder.CreateDynamicType("WhenNoMapping_ShouldTryToMapWithSameNames", Piece2);

            var piece1 = piece1Type.CreateObject()
                .SetValue("Length", 1.25M);
            var piece2 = piece2Type.CreateObject();

            // Act
            var result = Mapper.Instance.MapDefault(piece1, piece1.GetType(), piece2.GetType());

            // Assert
            Assert.Equal(piece1.GetValue("Length"), result.GetValue("Length"));
        }

        [Fact]
        public void WhenNoMapping_NoCommonPropertyNames_ShouldNotEqual()
        {
            // Arrange
            var piece1Type = CustomTypeBuilder.CreateDynamicType("WhenNoMapping_NoCommonPropertyNames_ShouldNotEqual", Piece1);
            var piece3Type = CustomTypeBuilder.CreateDynamicType("WhenNoMapping_NoCommonPropertyNames_ShouldNotEqual", Piece3);

            var piece1 = piece1Type.CreateObject()
                .SetValue("Length", 1.25M);
            var piece3 = piece3Type.CreateObject();

            // Act
            var result = Mapper.Instance.MapDefault(piece1, piece1.GetType(), piece3.GetType());
            
            // Assert
            Assert.NotEqual(piece1.GetValue("Length"), result.GetValue("Width"));
        }

        private static ClassGeneration Piece1 => new ClassGeneration { TypeName = "Piece1", Properties = new List<Property>(new []
        {
            new Property("Length", typeof(decimal)) 
        })};

        private static ClassGeneration Piece2 => new ClassGeneration { TypeName = "Piece2", Properties = new List<Property>(new []
        {
            new Property("Length", typeof(decimal)) 
        })};
        
        private static ClassGeneration Piece3 => new ClassGeneration { TypeName = "Piece3", Properties = new List<Property>(new []
        {
            new Property("Width", typeof(decimal)) 
        })};
    }
}