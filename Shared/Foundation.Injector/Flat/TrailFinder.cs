using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Foundation.Injector.Flat
{
    public static class TrailFinder
    {
        public static IEnumerable<IList<string>> GetTrails(string flatPropertyName,
            IEnumerable<PropertyInfo> lookupProps, Func<string, PropertyInfo, bool> match, StringComparison comparison,
            bool forFlattening = true)
        {
            var lookupObject = (object) null;
            return lookupProps.SelectMany(lp =>
                GetTrailsForProperty(flatPropertyName, lookupObject, lp, match, comparison, forFlattening));
        }

        public static IEnumerable<IList<string>> GetTrails(string flatPropertyName,
            object lookupObject, Func<string, PropertyInfo, bool> match, StringComparison comparison, bool forFlattening = true)
        {
            var lp = lookupObject.GetProps();
            return lp.SelectMany(lookup =>
                GetTrailsForProperty(flatPropertyName, lookupObject, lookup, match, comparison, forFlattening));
        }

        private static IEnumerable<IList<string>> GetTrailsForProperty(string flatPropName, object lookupObject,
            PropertyInfo lookupProp, Func<string, PropertyInfo, bool> match, StringComparison comparison,
            bool forFlattening = true)
        {
            if (forFlattening && !lookupProp.CanRead || !forFlattening && !lookupProp.CanWrite)
            {
                yield return null;
                yield break;
            }

            if (match(flatPropName, lookupProp))
            {
                yield return new List<string> {lookupProp.Name};
                yield break;
            }

            if (!flatPropName.StartsWith(lookupProp.Name, comparison)) yield break;
            
            if (lookupObject != null)
                lookupObject = lookupProp.GetValue(lookupObject);

            var props = lookupObject == null ? lookupProp.PropertyType.GetProps() : lookupObject.GetProps();

            foreach (var pro in props)
            {
                foreach (var trail in GetTrailsForProperty(
                    StringHelpers.RemovePrefix(flatPropName, lookupProp.Name, comparison), lookupObject, pro, match,
                    comparison, forFlattening))
                {
                    if (trail == null) continue;

                    var result = new List<string> {lookupProp.Name};
                    result.AddRange(trail);
                    yield return result;
                }
            }
        }
    }
}