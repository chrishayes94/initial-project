using System;
using System.Collections.Generic;
using System.Reflection;

namespace Foundation.Injector.Flat
{
    public static class Tunnelier
    {
        public static PropertyWithComponent Digg(IList<string> trail, object target,
            Func<PropertyInfo, object, object> activator = null)
        {
            while (true)
            {
                var type = target.GetType();
                if (trail.Count == 1)
                    return new PropertyWithComponent {Component = target, Property = type.GetProperty(trail[0])};

                var prop = type.GetProperty(trail[0]);
                var val = prop.GetValue(target, null);

                if (val == null)
                {
                    val = activator == null ? Activator.CreateInstance(prop.PropertyType) : activator(prop, target);
                    prop.SetValue(target, val, null);
                }

                trail.RemoveAt(0);
                target = val;
            }
        }

        public static PropertyWithComponent Find(IList<string> trail, object target) =>
            FindTargetInfo(trail, target, 0);

        private static PropertyWithComponent FindTargetInfo(IList<string> trail, object target, int level)
        {
            while (true)
            {
                var type = target.GetType();

                if (trail.Count == 1)
                    return new PropertyWithComponent
                        {Component = target, Property = type.GetProperty(trail[0]), Level = level};

                var prop = type.GetProperty(trail[0]);
                var val = prop.GetValue(target, null);
                if (val == null) return null;

                trail.RemoveAt(0);
                target = val;
                level += 1;
            }
        }
    }
}