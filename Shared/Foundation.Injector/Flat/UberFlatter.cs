using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Foundation.Injector.Flat
{
    public static class UberFlatter
    {
        public static IEnumerable<PropertyWithComponent> Unflat(string flatPropertyName, object lookupObject,
            Func<string, PropertyInfo, bool> match, StringComparison comparison,
            Func<PropertyInfo, object, object> activator = null)
        {
            var trails = TrailFinder.GetTrails(flatPropertyName, lookupObject, match, comparison, false)
                .Where(o => o != null);

            return trails.Select(trail => Tunnelier.Digg(trail, lookupObject, activator));
        }

        public static IEnumerable<PropertyWithComponent> Unflat(string flatPropertyName, object lookupObject,
            Func<string, PropertyInfo, bool> match, Func<PropertyInfo, object, object> activator = null)
        {
            return Unflat(flatPropertyName, lookupObject, match, StringComparison.Ordinal, activator);
        }

        public static IEnumerable<PropertyWithComponent> Unflat(string flatPropertyName, object lookupObject,
            Func<PropertyInfo, object, object> activator = null)
        {
            return Unflat(flatPropertyName, lookupObject, (upn, pi) => upn == pi.Name, activator);
        }

        public static IEnumerable<PropertyWithComponent> Flat(string flatPropertyName, object lookupObject,
            Func<string, PropertyInfo, bool> match)
        {
            return Flat(flatPropertyName, lookupObject, match, StringComparison.Ordinal);
        }

        public static IEnumerable<PropertyWithComponent> Flat(string flatPropertyName, object lookupObject,
            Func<string, PropertyInfo, bool> match, StringComparison comparison)
        {
            var trails = TrailFinder.GetTrails(flatPropertyName, lookupObject, match, comparison).Where(o => o != null);

            return trails.Select(trail => Tunnelier.Find(trail, lookupObject));
        }

        public static IEnumerable<PropertyWithComponent> Flat(string flatPropertyName, object lookupObject)
        {
            return Flat(flatPropertyName, lookupObject, (up, pi) => up == pi.Name);
        }
    }
}