using System;
using System.Collections.Concurrent;

namespace Foundation.Injector
{
    public interface IMapper
    {
        ConcurrentDictionary<Tuple<Type, Type>, Tuple<object, bool>> Maps { get; }

        Func<object, Type, object, object> DefaultMap { get; set; }

        TResult MapDefault<TResult>(object source, object tag = null);

        object MapDefault(object source, Type sourceType, Type targetType, object tag = null);

        TResult Map<TResult>(object source, object tag = null);

        TTarget Map<TSource, TTarget>(TSource source, object tag = null);

        void AddMap<TSource, TTarget>(Func<TSource, TTarget> func);

        void AddMap(Func<object, object> func, Type sourceType, Type targetType);

        void AddMap<TSource, TTarget>(Func<TSource, object, TTarget> func);

        void AddMap(Func<object, object, object> func, Type sourceType, Type targetType);

        void Reset();
    }
}