using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Foundation.Injector.Flat;

namespace Foundation.Injector.Injections
{
    public class FlatLoopInjection :  ValueInjector
    {
        protected override void Inject(object source, object target)
        {
            var targetProps = target.GetType().GetProps();
            foreach (var tp in targetProps)
                Execute(tp, source, target);
        }

        protected virtual bool Match(string propName, PropertyInfo unflatProp, PropertyInfo targetFlatProp) =>
            unflatProp.PropertyType == targetFlatProp.PropertyType &&
            propName == unflatProp.Name && unflatProp.GetSetMethod() != null;

        protected virtual void SetValue(object source, object target, PropertyInfo sp, PropertyInfo tp) =>
            tp.SetValue(target, sp.GetValue(source, null), null);

        protected void Execute(PropertyInfo tp, object source, object target)
        {
            if (tp.CanWrite && tp.GetSetMethod() != null)
            {
                var endpoints = UberFlatter.Flat(tp.Name, source, (upn, prop) => Match(upn, prop, tp)).ToArray();

                if (!endpoints.Any()) return;
                
                var endpoint = endpoints.First();
                if (endpoint == null) return;
                SetValue(endpoint.Component, target, endpoint.Property, tp);
            }
        }
    }
}