namespace Foundation.Injector.Injections
{
    public interface INoSourceInjection<TTarget>
    {
        TTarget Map(TTarget target);
    }
}