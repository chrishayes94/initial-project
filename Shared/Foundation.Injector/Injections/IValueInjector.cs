namespace Foundation.Injector.Injections
{
    public interface IValueInjector<in TSource, TTarget>
    {
        TTarget Map(TSource source, TTarget target);
    }

    public interface IValueInjector : IValueInjector<object, object>
    {
    }
}