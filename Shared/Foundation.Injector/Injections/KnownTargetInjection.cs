namespace Foundation.Injector.Injections
{
    public abstract class KnownTargetInjection<TTarget> : ValueInjector<object, TTarget>
    {
    }
}