using System;
using System.Linq;
using System.Reflection;

namespace Foundation.Injector.Injections
{
    public class LoopInjection<TSource, TTarget> : PropertyInjection<TSource, TTarget>
    {
        public LoopInjection(string[] ignoredProps = null) : base(ignoredProps)
        {}

        protected virtual string GetTargetProp(string sourceName) => sourceName;

        protected virtual bool MatchTypes(Type source, Type target) => source == target;

        protected virtual void SetValue(TSource source, TTarget target, PropertyInfo sp, PropertyInfo tp)
        {
            var val = sp.GetValue(source, null);
            tp.SetValue(target, val, null);
        }

        protected override void Execute(PropertyInfo sp, TSource source, TTarget target)
        {
            if (!sp.CanRead || sp.GetGetMethod() == null ||
                _ignoredProps != null && _ignoredProps.Contains(sp.Name)) return;
            
            var tp = target.GetType().GetProperty(GetTargetProp(sp.Name));
            if (tp != null && tp.CanWrite && tp.GetSetMethod() != null &&
                MatchTypes(sp.PropertyType, tp.PropertyType))
            {
                SetValue(source, target, sp, tp);
            }
        }
    }

    public class LoopInjection : LoopInjection<object, object>
    {
    }
}