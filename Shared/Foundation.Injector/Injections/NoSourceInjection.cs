namespace Foundation.Injector.Injections
{
    public abstract class NoSourceInjection<TTarget> : INoSourceInjection<TTarget>
    {
        public TTarget Map(TTarget target)
        {
            Inject(target);
            return target;
        }

        protected abstract void Inject(TTarget target);
    }

    public abstract class NoSourceInjection : NoSourceInjection<object>
    {
    } 
}