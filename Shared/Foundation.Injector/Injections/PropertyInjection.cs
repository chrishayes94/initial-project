using System.Linq;
using System.Reflection;

namespace Foundation.Injector.Injections
{
    public class PropertyInjection : PropertyInjection<object, object>
    {
    }

    public class PropertyInjection<TSource, TTarget> : ValueInjector<TSource, TTarget>
    {
        protected string[] _ignoredProps;

        public PropertyInjection(string[] ignoredProps = null)
        {
            _ignoredProps = ignoredProps;
        }
        
        protected override void Inject(TSource source, TTarget target)
        {
            var sourceProps = source.GetType().GetProps();
            foreach (var sp in sourceProps)
                Execute(sp, source, target);
        }

        protected virtual void Execute(PropertyInfo sp, TSource source, TTarget target)
        {
            if (!sp.CanRead || sp.GetSetMethod() == null ||
                _ignoredProps != null && _ignoredProps.Contains(sp.Name)) return;
            
            var tp = target.GetType().GetProperty(sp.Name);
            if (tp != null && tp.CanWrite && tp.PropertyType == sp.PropertyType && tp.GetSetMethod() != null)
                tp.SetValue(target, sp.GetValue(source, null), null);
        }
    }
}