namespace Foundation.Injector.Injections
{
    internal class SameNameType<TSource, TTarget> : ValueInjector<TSource, TTarget>
    {
        protected override void Inject(TSource source, TTarget target)
        {
            var sourceProps = source.GetProps();
            var targetType = target.GetType();

            foreach (var sp in sourceProps)
            {
                if (!sp.CanRead || sp.GetSetMethod() == null) continue;
                
                var tp = targetType.GetProperty(sp.Name);
                if (tp != null && tp.CanWrite && sp.PropertyType == tp.PropertyType && tp.GetSetMethod() != null)
                    tp.SetValue(target, sp.GetValue(source, null), null);
            }
        }
    }

    internal class SameNameType : SameNameType<object, object>
    {
    }
}