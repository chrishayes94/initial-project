using System;
using System.Linq;
using System.Reflection;
using Foundation.Injector.Flat;

namespace Foundation.Injector.Injections
{
    public class UnflatLoopInjection<TSource, TTarget> : ValueInjector<TSource, TTarget>
    {
        protected readonly Func<PropertyInfo, object, object> Activator;

        public UnflatLoopInjection(Func<PropertyInfo, object, object> activator = null)
        {
            Activator = activator;
        }
        
        protected override void Inject(TSource source, TTarget target)
        {
            var sourceProps = source.GetType().GetProps();
            foreach (var sp in sourceProps)
                Execute(sp, source, target);
        }

        protected virtual bool Match(string propName, PropertyInfo unflatProp, PropertyInfo sourceFlatProp) =>
            unflatProp.PropertyType == sourceFlatProp.PropertyType && propName == unflatProp.Name &&
            unflatProp.GetSetMethod() != null;

        protected virtual void SetValue(object source, object target, PropertyInfo sp, PropertyInfo tp) =>
            tp.SetValue(target, sp.GetValue(source, null), null);

        protected virtual void Execute(PropertyInfo sp, TSource source, TTarget target)
        {
            if (!sp.CanRead || sp.GetGetMethod() == null) return;
            var endpoints = UberFlatter.Unflat(sp.Name, target, (upn, prop) => Match(upn, prop, sp), Activator)
                .ToArray();

            foreach (var endpoint in endpoints)
                SetValue(source, endpoint.Component, sp, endpoint.Property);
        }
    }

    public class UnflatLoopInjection : UnflatLoopInjection<object, object>
    {
    }
}