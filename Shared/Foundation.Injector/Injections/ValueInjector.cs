namespace Foundation.Injector.Injections
{
    public abstract class ValueInjector : IValueInjector
    {
        public object Map(object source, object target)
        {
            Inject(source, target);
            return target;
        }

        protected abstract void Inject(object source, object target);
    }

    public abstract class ValueInjector<TSource, TTarget> : IValueInjector<TSource, TTarget>
    {
        public TTarget Map(TSource source, TTarget target)
        {
            Inject(source, target);
            return target;
        }

        protected abstract void Inject(TSource source, TTarget target);
    }
}