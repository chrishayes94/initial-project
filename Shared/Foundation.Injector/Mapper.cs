using System;
using System.Collections.Concurrent;

namespace Foundation.Injector
{
    public class Mapper : IMapper
    {
        public static IMapper Instance = new Mapper();
        
        internal Mapper()
        {
        }
        
        public ConcurrentDictionary<Tuple<Type, Type>, Tuple<object, bool>> Maps { get; } =
            new ConcurrentDictionary<Tuple<Type, Type>, Tuple<object, bool>>();

        public Func<object, Type, object, object> DefaultMap { get; set; } =
            (source, type, tag) => Activator.CreateInstance(type).InjectFrom(source);

        public TResult MapDefault<TResult>(object source, object tag = null) =>
            (TResult) DefaultMap(source, typeof(TResult), tag);

        public object MapDefault(object source, Type sourceType, Type targetType, object tag = null) =>
            DefaultMap(source, targetType, tag);

        public TResult Map<TResult>(object source, object tag = null)
        {
            var sourceType = source.GetType();

            if (!Maps.TryGetValue(new Tuple<Type, Type>(sourceType, typeof(TResult)), out var funct))
                return (TResult) DefaultMap(source, typeof(TResult), tag);

            var parameters = funct.Item2 ? new[] {source, tag} : new[] {source};
            return (TResult) funct.Item1.GetType().GetMethod("Invoke")?.Invoke(funct.Item1, parameters);
        }

        public TTarget Map<TSource, TTarget>(TSource source, object tag = null)
        {
            if (!Maps.TryGetValue(new Tuple<Type, Type>(typeof(TSource), typeof(TTarget)), out var funct))
                return (TTarget) DefaultMap(source, typeof(TTarget), tag);

            if (funct.Item2) return ((Func<TSource, object, TTarget>) funct.Item1)(source, tag);
            return ((Func<TSource, TTarget>) funct.Item1)(source);
        }

        public void AddMap<TSource, TTarget>(Func<TSource, TTarget> func)
        {
            Maps.AddOrUpdate(new Tuple<Type, Type>(typeof(TSource), typeof(TTarget)),
                new Tuple<object, bool>(func, false), (key, oldValue) => new Tuple<object, bool>(func, false));
        }
        
        public void AddMap(Func<object, object> func, Type sourceType, Type targetType)
        {
            Maps.AddOrUpdate(new Tuple<Type, Type>(sourceType, targetType),
                new Tuple<object, bool>(func, false), (key, oldValue) => new Tuple<object, bool>(func, false));
        }

        public void AddMap<TSource, TTarget>(Func<TSource, object, TTarget> func)
        {
            Maps.AddOrUpdate(new Tuple<Type, Type>(typeof(TSource), typeof(TTarget)),
                new Tuple<object, bool>(func, true), (key, oldValue) => new Tuple<object, bool>(func, true));
        }

        public void AddMap(Func<object, object, object> func, Type sourceType, Type targetType)
        {
            Maps.AddOrUpdate(new Tuple<Type, Type>(sourceType, targetType), new Tuple<object, bool>(func, true),
                (key, oldValue) => new Tuple<object, bool>(func, true));
        }

        public void Reset()
        {
            Maps.Clear();
            DefaultMap = (source, type, tag) => Activator.CreateInstance(type).InjectFrom(source);
        }
    }
}