﻿using System;
using Foundation.Injector.Injections;

namespace Foundation.Injector
{
    public static class StaticValueInjector
    {
        public static IValueInjector<object, object> DefaultInjection = new SameNameType<object, object>();

        public static object InjectFrom<T>(this object target, object source)
            where T : IValueInjector<object, object>, new() =>
            target = new T().Map(source, target);

        public static object InjectFrom(this object target, IValueInjector<object, object> injection, object source) =>
            injection.Map(source, target);

        public static object InjectFrom<T>(this object target) where T : INoSourceInjection<object>, new() =>
            new T().Map(target);

        public static object InjectFrom(this object target, INoSourceInjection<object> injection) =>
            injection.Map(target);

        public static object InjectFrom(this object target, object source) => DefaultInjection.Map(source, target);
    }
}