using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Foundation.Mediator;
using Foundation.ModelGeneration;
using Foundation.ModelGeneration.Events;
using Foundation.ModelGeneration.Grains;
using Foundation.ModelGeneration.Interfaces;
using Orleans.TestKit;
using Xunit;

namespace Foundation.Tests
{
    public class ClassManagementTests : TestKitBase, IAsyncRequestHandler<LookupSupplierTypes, IEnumerable<ClassGeneration>>
    {
        public ClassManagementTests()
        {
            var mediator = new FakeMediator();
            mediator.AddSendHandler(this);
            AddService<IMediator>(mediator);
        }

        private void AddService<TImplementation>(TImplementation implementation)
        {
            Silo.ServiceProvider.AddService(implementation);
        }
        
        [Fact]
        public async Task WhenGrainIsCreated_ShouldPopulate_EmittedTypes()
        {
            // Arrange
            
            var classManagement = await Silo.CreateGrainAsync<ClassManagementGrain>("Norsk");
            
            // Act
            var dynamicallyEmittedType = await classManagement.Get("Piece");
            dynamicallyEmittedType.SetValue("Length", 1.00m);
            
            // Assert
            Assert.Equal(1.00m, dynamicallyEmittedType.GetValue("Length"));
        }

        public Task<IEnumerable<ClassGeneration>> Handle(LookupSupplierTypes message)
        {
            var piece = new ClassGeneration
                {TypeName = "Piece", Properties = new List<Property>(new[] {new Property("Length", typeof(decimal)),})};
            
            return Task.FromResult(new List<ClassGeneration>(new [] {piece}).AsEnumerable());
        }
    }
    
    internal class FakeMediator : IMediator
    {
        private readonly Dictionary<Type, object> _sendObjects = new Dictionary<Type, object>();
        private readonly Dictionary<Type, object> _publishObjects = new Dictionary<Type, object>();

        public void AddSendHandler<TRequest, TResponse>(IAsyncRequestHandler<TRequest, TResponse> handler)
            where TRequest : IAsyncRequest<TResponse>
        {
            _sendObjects.Add(typeof(IAsyncRequest<TResponse>), handler);
        }

        public Task<TResponse> SendAsync<TResponse>(IAsyncRequest<TResponse> request)
        {
            if (!_sendObjects.TryGetValue(typeof(IAsyncRequest<TResponse>), out var @object))
                throw new NotImplementedException(
                    $"No Handler has been setup for {typeof(IAsyncRequest<TResponse>).Name}");

            var implementationType = GetImplementationType(typeof(IAsyncRequest<TResponse>));
            var handleMethod = @object.GetType().GetMethod("Handle", new[] {implementationType});

            if (handleMethod == null)
                throw new NotImplementedException(
                    $"Can not find implementation method IAsyncRequest {implementationType.Name}");
            
            var result = handleMethod.Invoke(@object, new object[] {request});
            return (Task<TResponse>) result;
        }

        public TResponse Send<TResponse>(IRequest<TResponse> request)
        {
            return default;
        }

        public Task PublishAsync<TNotification>(TNotification notification) where TNotification : IAsyncNotification
        {
            return Task.CompletedTask;
        }

        private static Type GetImplementationType(Type type)
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(a => a.GetTypes())
                .First(a => a.GetInterfaces().Contains(type) && !a.IsInterface && !a.IsAbstract);
        }
    }
}