using System.Threading.Tasks;
using Foundation.Mediator;

namespace Foundation.Tests.Fixtures
{
    public class TimesAByB : IRequest<int>
    {
        public int A { get; set; }

        public int B { get; set; }
    }

    public class AsyncTimesAByB : IAsyncRequest<int>
    {
        public int A { get; set; }

        public int B { get; set; }
    }
    
    public class MathHandler : IAsyncRequestHandler<AsyncTimesAByB, int>, IRequestHandler<TimesAByB, int>
    {
        public Task<int> Handle(AsyncTimesAByB message)
        {
            return Task.FromResult(message.A * message.B);
        }

        public int Handle(TimesAByB message)
        {
            return message.A * message.B;
        }
    }
}