using System;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Foundation.DependencyInjection;
using Foundation.Mediator;
using Foundation.Tests.Fixtures;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Foundation.Tests
{
    public class MediatorTests : IRegistory
    {
        private readonly IServiceProvider _provider;

        private IMediator Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<RegistoryAdapator<MediatorRegistory>>();
            builder.RegisterModule<RegistoryAdapator<MediatorTests>>();
            var container = builder.Build();

            //_provider = container.Resolve<IServiceProvider>();
            return container.Resolve<IMediator>();
        }

        [Fact]
        public async Task When_UsingAsyncRequestWrapper_ShouldMultiplyValues()
        {
            // Arrange
            var mediator = Setup();
            const int a = 4;
            const int b = 4;
            const int result = 16;
            var request = new AsyncTimesAByB {A = a, B = b};
            
            // Act
            var requestResult = await  mediator.SendAsync(request);
            
            // Assert
            Assert.Equal(result, requestResult);
        }

        [Fact]
        public void When_UsingRequestWrapper_ShouldMultiplyValues()
        {
            // Arrange
            var mediator = Setup();
            const int a = 4;
            const int b = 4;
            const int result = 16;
            var request = new TimesAByB {A = a, B = b};
            
            // Act
            var requestResult =  mediator.Send(request);
            
            // Assert
            Assert.Equal(result, requestResult);
        }

        public void Load(IRegistoryBuilder builder)
        {
            builder.Scan(typeof(MediatorTests).Assembly, scan => scan.ImplementedInterfaces());
            builder.Singleton<Mediator.Mediator, IMediator>();
        }
    }
}