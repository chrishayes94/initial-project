namespace Foundation.DependencyInjection
{
    public interface IContainerProvider
    {
        IRegistoryBuilder Builder();

        IContainerScope Build();
    }
}