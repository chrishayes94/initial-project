using System;
using System.Collections.Generic;

namespace Foundation.DependencyInjection
{
    public interface IContainerScope
    {
        object Resolve(Type handlerType);

        IEnumerable<T> ResolveMany<T>();

        IContainerScope ChildScope();

        T Resolve<T>() where T : class;
    }
}