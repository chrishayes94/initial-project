namespace Foundation.DependencyInjection
{
    public interface IRegistory
    {
        void Load(IRegistoryBuilder builder);
    }
}