using System;
using System.Reflection;
using System.Threading.Tasks;

namespace Foundation.DependencyInjection
{
    public interface IRegistoryBuilder
    {
        IRegistoryBuilder IncludeModule<T>() where T : IRegistory, new();

        IRegistoryBuilder Scan(Assembly assembly, Action<IRegistoryScanner> config);

        IRegistoryBuilder Scan(Assembly[] assemblies, Action<IRegistoryScanner> config);

        IRegistoryBuilder Transient(Type serviceType, Func<IRegistoryContext, object> activator);

        IRegistoryBuilder Singleton<T>();
        
        IRegistoryBuilder Singleton<T>(Func<IRegistoryContext, T> p);

        IRegistoryBuilder SingletonIfNone<T>(Func<IRegistoryContext, T> p);

        IRegistoryBuilder Singleton<TImplementation, TInterface>() where TImplementation : TInterface;

        IRegistoryBuilder SingletonIfNone<TImplementation, TInterface>(Func<IRegistoryContext, Task<TImplementation>> p)
            where TImplementation : TInterface;

        IRegistoryBuilder Transient(Type concreteType, Type serviceType);

        IRegistoryBuilder Transient<T>();

        IRegistoryBuilder Transient<T>(Func<IRegistoryContext, T> p);

        IRegistoryBuilder TransientGeneric(Type source, Type target);

        IRegistoryBuilder Transient<TImplementation, TInterface>() where TImplementation : TInterface;

        IRegistoryBuilder Transient<TImplementation, TInterface>(Func<IRegistoryContext, TInterface> action)
            where TImplementation : TInterface;

        IRegistoryBuilder Transient<TImplementation, TInterface>(Func<IRegistoryContext, Task<TInterface>> action)
            where TImplementation : TInterface;
    }
}