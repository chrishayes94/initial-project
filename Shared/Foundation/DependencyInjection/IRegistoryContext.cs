namespace Foundation.DependencyInjection
{
    public interface IRegistoryContext
    {
        T Resolve<T>() where T : class;

        T ResolveNamed<T>(string name) where T : class;
    }
}