using System;

namespace Foundation.DependencyInjection
{
    public interface IRegistoryScanner
    {
        void ImplementedInterfaces();

        void AssignableAsSelf<T>();

        void ClosedTypeAsSelf(Type type);

        void ByNameAsSelf(string partOfName);

        IRegistoryScanner Except<T>();

        IRegistoryScanner Ignore(string typeOrName);
    }
}