using System;
using System.Collections.Generic;

namespace Foundation.Helpers
{
    public static class TypeHelper
    {
        public static Type AsType(this string input)
        {
            switch (input)
            {
                case "Integer":
                    return typeof(int);
                case "Long":
                    return typeof(long);
                case "Short":
                    return typeof(short);
                case "Decimal":
                    return typeof(decimal);
                case "String":
                    return typeof(string);
                case "Char":
                    return typeof(char);
                case "Array":
                    return typeof(Array);
                case "List":
                    return typeof(List<>);
                default:
                    return Type.GetType(input);
            }
        }
        
        public static bool IsSimpleType(this Type typeToCheck)
        {
            var typeCode = Type.GetTypeCode(GetUnderlyingType(typeToCheck));

            switch (typeCode)
            {
                case TypeCode.Boolean:
                case TypeCode.Byte:
                case TypeCode.Char:
                case TypeCode.DateTime:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.SByte:
                case TypeCode.Single:
                case TypeCode.String:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                    return true;
                default:
                    return false;
            }
        }    
        
        private static Type GetUnderlyingType(Type typeToCheck)
        {
            if (typeToCheck.IsGenericType &&
                typeToCheck.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                return Nullable.GetUnderlyingType(typeToCheck);
            }
            else
            {
                return typeToCheck;
            }
        }
    }
}