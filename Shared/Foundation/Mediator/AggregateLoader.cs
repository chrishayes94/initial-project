using System.Threading.Tasks;

namespace Foundation.Mediator
{
    public interface IAggregateLoader<T> where T : AggregateRoot 
    {
        Task<T> Load(long id);
    }

    public interface IAggregateRequest<out T> : IAsyncRequest<T> where T : AggregateRoot
    {
        long Id { get; }
    }

    public class AggregateAsyncAdaptor<THandler, T> : IAsyncRequestHandler<THandler, T> where T : AggregateRoot
        where THandler : IAsyncRequest<T>, IAggregateRequest<T>
    {
        private readonly IAggregateLoader<T> _handler;

        public AggregateAsyncAdaptor(IAggregateLoader<T> handler)
        {
            _handler = handler;
        }

        public async Task<T> Handle(THandler message)
        {
            return await _handler.Load(message.Id);
        }
    }
    
    public class AggregateAdaptor<THandler, T> : IRequestHandler<THandler, T> where T : AggregateRoot
        where THandler : IRequest<T>, IAggregateRequest<T>
    {
        private readonly IAggregateLoader<T> _handler;

        public AggregateAdaptor(IAggregateLoader<T> handler)
        {
            _handler = handler;
        }

        public T Handle(THandler message)
        {
            return _handler.Load(message.Id).GetAwaiter().GetResult();
        }
    }
}