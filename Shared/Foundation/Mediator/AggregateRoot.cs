using System.Collections.Generic;

namespace Foundation.Mediator
{
    public class AggregateRoot
    {
        public IReadOnlyList<IDomainEvent> Events => _events.AsReadOnly();
        private List<IDomainEvent> _events = new List<IDomainEvent>();

        public void Raise<T>(T domainEvent) where T : IDomainEvent
        {
            _events.Add(domainEvent);
        }
    }
}