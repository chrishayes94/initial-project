using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;

namespace Foundation.Mediator
{
    public class DefaultHandlerProvider : IHandlerProvider
    {
        private readonly ILifetimeScope _scope;

        public DefaultHandlerProvider(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public RequestWrapper<TResponse> GetHandler<TResponse>(IRequest<TResponse> request)
        {
            var handlerType = typeof(IRequestHandler<,>).MakeGenericType(request.GetType(), typeof(TResponse));
            var wrapperType = typeof(RequestWrapper<,>).MakeGenericType(request.GetType(), typeof(TResponse));
            object handler;

            if ((handler = _scope.Resolve(handlerType)) != null)
            {
                var wrapperHandler = Activator.CreateInstance(wrapperType, handler);
                return (RequestWrapper<TResponse>) wrapperHandler;
            }

            return null;
        }
        
        public AsyncRequestWrapper<TResponse> GetHandler<TResponse>(IAsyncRequest<TResponse> request)
        {
            var handlerType = typeof(IAsyncRequestHandler<,>).MakeGenericType(request.GetType(), typeof(TResponse));
            var wrapperType = typeof(AsyncRequestWrapper<,>).MakeGenericType(request.GetType(), typeof(TResponse));
            object handler;

            if ((handler = _scope.ResolveOptional(handlerType)) != null)
            {
                var wrapperHandler = Activator.CreateInstance(wrapperType, handler);
                return (AsyncRequestWrapper<TResponse>) wrapperHandler;
            }

            return null;
        }

        public IEnumerable<IAsyncNotificationHandler<TNotification>> GetNotificationHandlers<TNotification>() where TNotification : IAsyncNotification
        {
            return _scope.Resolve<IEnumerable<IAsyncNotificationHandler<TNotification>>>().ToArray();
        }

        public IEnumerable<IAsyncNotificationHandler<TNotification>> GetAsyncNotificationHandlers<TNotification>() where TNotification : IAsyncNotification
        {
            return _scope.Resolve<IEnumerable<IAsyncNotificationHandler<TNotification>>>().ToArray();
        }
    }
}