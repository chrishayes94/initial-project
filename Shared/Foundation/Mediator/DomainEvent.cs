namespace Foundation.Mediator
{
    public interface IDomainEvent : IAsyncNotification
    {
    }

    public class DomainEventContext
    {
        private readonly IMediator _mediator;
        
        public DomainEventContext(IMediator mediator)
        {
            _mediator = mediator;
        }

        public void Send(IAsyncNotification command) => _mediator.PublishAsync(command);
    }

    public interface IDomainEvent<in T> : IDomainEvent where T : DomainEventContext
    {
        void Record(T context);
    }
}