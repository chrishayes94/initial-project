using System.Threading.Tasks;

namespace Foundation.Mediator
{
    public interface IAsyncRequestWrapper<TResult>
    {
        Task<TResult> Handle(IAsyncRequest<TResult> message);
    }

    public interface IRequestWrapper<TResult>
    {
        TResult Handle(IRequest<TResult> message);
    }
}