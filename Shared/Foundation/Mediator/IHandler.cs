using System.Threading.Tasks;

namespace Foundation.Mediator
{
    public interface IAsyncRequestHandler<in TRequest, TResponse> where TRequest : IAsyncRequest<TResponse>
    {
        Task<TResponse> Handle(TRequest message);
    }

    public interface IRequestHandler<in TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        TResponse Handle(TRequest message);
    }
    
    public abstract class RequestHandler<TMessage> : IRequestHandler<TMessage, Unit>
        where TMessage : IRequest<Unit>
    {
        public Unit Handle(TMessage message)
        {
            HandleCore(message);
            return Unit.Value;
        }

        protected abstract void HandleCore(TMessage message);
    }

    public abstract class AsyncRequestHandler<TMessage> : IAsyncRequestHandler<TMessage, Unit>
        where TMessage : IAsyncRequest<Unit>
    {
        public async Task<Unit> Handle(TMessage message)
        {
            await HandleCore(message);

            return Unit.Value;
        }

        protected abstract Task HandleCore(TMessage message);
    }

    public interface IAsyncNotificationHandler<in TNotification> where TNotification : IAsyncNotification
    {
        Task Handle(TNotification notification);
    }
}