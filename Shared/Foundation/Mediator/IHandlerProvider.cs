using System.Collections.Generic;
using System.Threading.Tasks;

namespace Foundation.Mediator
{
    public interface IHandlerProvider
    {
        AsyncRequestWrapper<TResponse> GetHandler<TResponse>(IAsyncRequest<TResponse> request);

        RequestWrapper<TResponse> GetHandler<TResponse>(IRequest<TResponse> request);

        IEnumerable<IAsyncNotificationHandler<TNotification>> GetNotificationHandlers<TNotification>()
            where TNotification : IAsyncNotification;
        
        IEnumerable<IAsyncNotificationHandler<TNotification>> GetAsyncNotificationHandlers<TNotification>()
            where TNotification : IAsyncNotification;

    }

    public abstract class RequestWrapper<TResult> : IRequestWrapper<TResult>
    {
        public abstract TResult Handle(IRequest<TResult> message);
    }

    public class RequestWrapper<TCommand, TResult> : RequestWrapper<TResult> where TCommand : IRequest<TResult>
    {
        private readonly IRequestHandler<TCommand, TResult> _inner;

        public RequestWrapper(IRequestHandler<TCommand, TResult> inner)
        {
            _inner = inner;
        }

        public override TResult Handle(IRequest<TResult> message)
        {
            return _inner.Handle((TCommand) message);
        }
    }

    public abstract class AsyncRequestWrapper<TResult> : IAsyncRequestWrapper<TResult>
    {
        public abstract Task<TResult> Handle(IAsyncRequest<TResult> message);
    }

    public class AsyncRequestWrapper<TCommand, TResult> : AsyncRequestWrapper<TResult>
        where TCommand : IAsyncRequest<TResult>
    {
        private readonly IAsyncRequestHandler<TCommand, TResult> _inner;
        
        public AsyncRequestWrapper(IAsyncRequestHandler<TCommand, TResult> inner)
        {
            _inner = inner;
        }

        public override async Task<TResult> Handle(IAsyncRequest<TResult> message)
        {
            return await _inner.Handle((TCommand) message);
        }
    }
}