using System.Threading.Tasks;

namespace Foundation.Mediator
{
    public interface IMediator
    {
        Task<TResponse> SendAsync<TResponse>(IAsyncRequest<TResponse> request);

        TResponse Send<TResponse>(IRequest<TResponse> request);

        Task PublishAsync<TNotification>(TNotification notification) where TNotification : IAsyncNotification;
    }
}