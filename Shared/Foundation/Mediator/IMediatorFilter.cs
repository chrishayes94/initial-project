using System;
using System.Threading.Tasks;

namespace Foundation.Mediator
{
    public interface IMediatorFilter
    {
        Task<TResponse> SendAsync<TResponse>(IAsyncRequest<TResponse> request,
            Func<IAsyncRequest<TResponse>, Task<TResponse>> next);

        TResponse Send<TResponse>(IRequest<TResponse> request, Func<IRequest<TResponse>, TResponse> next);

        Task PublishAsync<TNotification>(TNotification notification, Func<TNotification, Task> next)
            where TNotification : IAsyncNotification;
    }
}