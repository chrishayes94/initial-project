namespace Foundation.Mediator
{
    public interface IRequest<out TResult>
    {
    }

    public interface IAsyncRequest<out TResult>
    {
    }

    public interface IAsyncNotification
    {
    }

    public interface INotification
    {
    }
}