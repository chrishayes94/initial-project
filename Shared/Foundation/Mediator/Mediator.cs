using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Foundation.Mediator
{
    public delegate object SingleInstanceFactory(Type serviceType);

    public delegate object MultiInstanceFactory(Type serviceType);
    
    public class Mediator : IMediator
    {
        private readonly IEnumerable<IHandlerProvider> _handlerProviders;

        public Mediator(IEnumerable<IHandlerProvider> handlerProviders)
        {
            _handlerProviders = handlerProviders;
        }

        public Task<TResponse> SendAsync<TResponse>(IAsyncRequest<TResponse> request) => SendAsyncImpl(request);

        private async Task<TResponse> SendAsyncImpl<TResponse>(IAsyncRequest<TResponse> request)
        {
            var defaultHandler = GetHandler(request);
            var result = await defaultHandler.Handle(request);
            return result;
        }

        public TResponse Send<TResponse>(IRequest<TResponse> request) => SendImpl(request);

        private TResponse SendImpl<TResponse>(IRequest<TResponse> request)
        {
            var defaultHandler = GetHandler(request);
            var result = defaultHandler.Handle(request);
            return result;
        }

        public Task PublishAsync<TNotification>(TNotification notification) where TNotification : IAsyncNotification
        {
            return Task.CompletedTask;
        }

        private AsyncRequestWrapper<TResponse> GetHandler<TResponse>(IAsyncRequest<TResponse> request)
        {
            foreach (var provider in _handlerProviders)
            {
                var handler = provider.GetHandler(request);
                if (handler != null) return handler;
            }

            throw new Exception();
        }

        private RequestWrapper<TResponse> GetHandler<TResponse>(IRequest<TResponse> request)
        {
            foreach (var provider in _handlerProviders)
            {
                var handler = provider.GetHandler(request);
                if (handler != null) return handler;
            }
            throw new Exception();
        }
    }
}