using System;
using Foundation.DependencyInjection;

namespace Foundation.Mediator
{
    public class MediatorRegistory : IRegistory
    {
        public void Load(IRegistoryBuilder builder)
        {
            
            builder.Transient<Mediator, IMediator>();
            builder.Transient<DefaultHandlerProvider, IHandlerProvider>();

            builder.TransientGeneric(
                typeof(AggregateAsyncAdaptor<,>),
                typeof(IAsyncRequestHandler<,>));

            builder.TransientGeneric(
                typeof(AggregateAdaptor<,>),
                typeof(IRequestHandler<,>));
        }
    }
}