using System;

namespace Foundation.Mediator
{
    public sealed class Unit : IComparable
    {
        public static readonly Unit Value = new Unit();

        public override int GetHashCode() => 0;
        
        public override bool Equals(object obj) => obj == null || obj is Unit;

        public int CompareTo(object obj) => 0;
    }
}