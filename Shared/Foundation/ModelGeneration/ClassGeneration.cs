using System.Collections.Generic;

namespace Foundation.ModelGeneration
{
    public class ClassGeneration
    {
        public string TypeName { get; set; }

        public List<Property> Properties { get; set; }
    }
}