namespace Foundation.ModelGeneration
{
    public enum ContentType
    {
        Json,
        Xml
    }
}