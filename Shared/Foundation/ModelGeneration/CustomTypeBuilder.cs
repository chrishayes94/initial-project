using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Foundation.ModelGeneration
{
    public static class CustomTypeBuilder
    {
        private static readonly Dictionary<string, AssemblyBuilder> DynamicAssemblies = new Dictionary<string, AssemblyBuilder>();

        public static object CreateObject(this TypeInfo type)
        {
            var dynamicType = type.AsType();
            var @object = Activator.CreateInstance(dynamicType);
            return @object;
        }

        public static TypeInfo CreateDynamicType(string assembly, ClassGeneration @class)
        {
            return CreateDynamicType(assembly, @class.TypeName, tb =>
            {
                foreach (var property in @class.Properties)
                    tb.CreateProperty(property.Key, property.Type);
            });
        }


        private static TypeInfo CreateDynamicType(string assembly, string type, Action<TypeBuilder> callback)
        {
            var typeBuilder = CreateDynamicType(assembly, type);
            typeBuilder.DefineDefaultConstructor(MethodAttributes.Public | MethodAttributes.SpecialName |
                                                 MethodAttributes.RTSpecialName);

            callback(typeBuilder);
            var objectTypeInfo = typeBuilder.CreateTypeInfo();
            return objectTypeInfo;
        }

        public static object SetValue(this object obj, string property, object value)
        {
            obj.GetType().GetProperty(property)?.SetValue(obj, value);
            return obj;
        }

        public static object GetValue(this object obj, string property) => obj.GetType().GetProperty(property)?.GetValue(obj);

        private static TypeBuilder CreateDynamicType(string assembly, string type)
        {
            if (!DynamicAssemblies.TryGetValue(assembly, out var dynamicAssembly))
            {
                dynamicAssembly =
                    AssemblyBuilder.DefineDynamicAssembly(
                        new AssemblyName($"ApiLibrary.ModelGeneration.Dynamic.{assembly}"),
                        AssemblyBuilderAccess.RunAndCollect);
                DynamicAssemblies.Add(assembly, dynamicAssembly);
            }

            var module = dynamicAssembly.GetDynamicModule(type) ??
                         dynamicAssembly.DefineDynamicModule(type);
            var tb = module.DefineType(type,
                TypeAttributes.Public |
                TypeAttributes.Class |
                TypeAttributes.AutoClass |
                TypeAttributes.AnsiClass |
                TypeAttributes.BeforeFieldInit |
                TypeAttributes.AutoLayout,
                null);
            return tb;
        }

        public static TypeBuilder CreateProperty(this TypeBuilder typeBuilder, string propertyName, Type propertyType)
        {
            var fieldBuilder = typeBuilder.DefineField("_" + propertyName, propertyType, FieldAttributes.Private);

            var propertyBuilder =
                typeBuilder.DefineProperty(propertyName, PropertyAttributes.HasDefault, propertyType, null);
            var getMethodBuilder = typeBuilder.DefineMethod("get_" + propertyName,
                MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig,
                propertyType, Type.EmptyTypes);

            var getGenerator = getMethodBuilder.GetILGenerator();
            getGenerator.Emit(OpCodes.Ldarg_0);
            getGenerator.Emit(OpCodes.Ldfld, fieldBuilder);
            getGenerator.Emit(OpCodes.Ret);

            var setMethodBuilder = typeBuilder.DefineMethod("set_" + propertyName,
                MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig, null,
                new[] {propertyType});

            var setGenerator = setMethodBuilder.GetILGenerator();
            var modifyProperty = setGenerator.DefineLabel();
            var exitSet = setGenerator.DefineLabel();

            setGenerator.MarkLabel(modifyProperty);
            setGenerator.Emit(OpCodes.Ldarg_0);
            setGenerator.Emit(OpCodes.Ldarg_1);
            setGenerator.Emit(OpCodes.Stfld, fieldBuilder);
            
            setGenerator.Emit(OpCodes.Nop);
            setGenerator.MarkLabel(exitSet);
            setGenerator.Emit(OpCodes.Ret);
            
            propertyBuilder.SetGetMethod(getMethodBuilder);
            propertyBuilder.SetSetMethod(setMethodBuilder);
            return typeBuilder;
        }
        
    }
}