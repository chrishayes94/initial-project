using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Foundation.Mediator;

namespace Foundation.ModelGeneration.Events
{
    public class LookupSupplierTypes : IAsyncRequest<IEnumerable<ClassGeneration>>
    {
        public string SupplierName { get; }

        public LookupSupplierTypes(string supplierName)
        {
            SupplierName = supplierName;
        }
    }
    
    public class ClassGenerationHandler : IAsyncRequestHandler<LookupSupplierTypes, IEnumerable<ClassGeneration>>
    {
        public Task<IEnumerable<ClassGeneration>> Handle(LookupSupplierTypes message)
        {
            return Task.FromResult(new List<ClassGeneration>().AsEnumerable());
        }
    }
}