using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Foundation.Mediator;
using Foundation.ModelGeneration.Events;
using Foundation.ModelGeneration.Interfaces;
using Orleans;

namespace Foundation.ModelGeneration.Grains
{
    public class ClassManagementGrain : Grain, IClassManagement
    {
        private readonly IMediator _mediator;
        private readonly Dictionary<string, TypeInfo> _emittedTypes = new Dictionary<string, TypeInfo>();

        public ClassManagementGrain(IMediator mediator)
        {
            _mediator = mediator;
        }

        public override async Task OnActivateAsync()
        {
            var rawTypes = await _mediator.SendAsync(new LookupSupplierTypes(this.GetPrimaryKeyString()));
            foreach (var raw in rawTypes)
                _emittedTypes.Add(raw.TypeName, CustomTypeBuilder.CreateDynamicType(this.GetPrimaryKeyString(), raw));
        }

        public Task<object> Get(string typeName)
        {
            if (_emittedTypes.TryGetValue(typeName, out var @class))
                return Task.FromResult(@class.CreateObject());
            throw new ObjectNotFoundException();
        }

        public Task Add(ClassGeneration generation)
        {
            if (_emittedTypes.ContainsKey(generation.TypeName))
                return Task.CompletedTask;
            _emittedTypes.Add(generation.TypeName, CustomTypeBuilder.CreateDynamicType(this.GetPrimaryKeyString(), generation));
            return Task.CompletedTask;
        }

        public Task<object> Map(object classModel)
        {
            throw new NotImplementedException();
        }
    }

    public class ObjectNotFoundException : Exception
    {
    }
}