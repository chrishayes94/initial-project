using System;

namespace Foundation.ModelGeneration
{
    public interface IProperty<TValue>
    {
        string Key { get; set; }

        Type Type { get; set; }
    }
}