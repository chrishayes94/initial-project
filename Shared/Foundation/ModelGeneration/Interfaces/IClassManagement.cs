using System.Threading.Tasks;
using Orleans;

namespace Foundation.ModelGeneration.Interfaces
{
    public interface IClassManagement : IGrainWithStringKey
    {
        Task<object> Get(string typeName);

        Task Add(ClassGeneration generation);

        Task<object> Map(object classModel);
    }
}