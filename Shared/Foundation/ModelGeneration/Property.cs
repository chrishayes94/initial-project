using System;

namespace Foundation.ModelGeneration
{
    public class Property : IProperty<object>
    {
        public Property(string key, Type type)
        {
            Key = key;
            Type = type;
        }

        public string Key { get; set; }

        public Type Type { get; set; }
    }
}